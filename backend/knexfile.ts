// Update with your config settings.
import environment from './src/environment';


module.exports = {
  development: {
    client: 'pg',
    connection: {
      host: environment.DATABASE_HOST,
      database: environment.DATABASE_DB,
      user: environment.DATABASE_USER,
      password: environment.DATABASE_PASSWORD,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },
  production: {
    client: 'pg',
    connection: {
      host: environment.DATABASE_HOST,
      database: environment.DATABASE_DB,
      user: environment.DATABASE_USER,
      password: environment.DATABASE_PASSWORD,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },
};
