import * as Knex from 'knex';


export async function up(knex: Knex): Promise<any> {
  return knex.schema.createTable('rentals', (table) => {
    table.increments();
    table.json('location').notNullable();
    table.string('quality').notNullable();
    table.timestamps(true, true);
  });
}


export async function down(knex: Knex): Promise<any> {
  return knex.schema.dropTable('rentals');
}
