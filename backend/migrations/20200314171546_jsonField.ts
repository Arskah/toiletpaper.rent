import * as Knex from 'knex';


export async function up(knex: Knex): Promise<any> {
  return knex.schema.table('rentals', (table) => {
    table.dropColumns('location', 'quality');
    table.json('jsonField').notNullable();
  });
}


export async function down(knex: Knex): Promise<any> {
  return knex.schema.table('rentals', (table) => {
    table.dropColumn('jsonField');
    table.json('location').notNullable();
    table.string('quality').notNullable();
  });
}
