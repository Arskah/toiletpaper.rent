import knex from 'knex';

// eslint-disable-next-line
const config = require('../knexfile');

export default knex((config as any)[process.env.NODE_ENV || 'development']);
