type Location = {
  latitude: number;
  longitude: number;
}

export enum PaperQuality {
  PREMIUM='PREMIUM',
  SERLA='SERLA',
  RECYCLED='RECYCLED',
  STYLED='STYLED',
  SAND_PAPER='SAND_PAPER'
}


export interface Rental {
  nickname: string;
  location: Location;
  quality: PaperQuality;
  extraDetails: string;
}


// eslint-disable-next-line
export const toDomain = (dbObject: any): Rental => {
  const {
    jsonField: {
      nickname, location, quality, extraDetails,
    },
  } = dbObject;
  return ({
    nickname,
    location,
    quality,
    extraDetails,
  });
};

// eslint-disable-next-line
export const toDbObject = (rental: Rental) => ({
  jsonField: JSON.stringify(rental),
});
