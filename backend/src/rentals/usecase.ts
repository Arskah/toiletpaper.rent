import { Rental, toDomain, toDbObject } from './domain';
import db from '../db';

const TABLE = 'rentals';

export const getAll = async (): Promise<Rental[]> => {
  try {
    const query = await db(TABLE).select('*');
    return query.map(toDomain);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    throw err;
  }
};

export const getByID = async (id: number): Promise<Rental> => {
  try {
    const query = await db(TABLE).where({ id }).first();
    return toDomain(query);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    throw err;
  }
};

export const saveNew = async (rental: Rental): Promise<Rental> => {
  try {
    const query = await db(TABLE).insert(toDbObject(rental)).returning('*');
    return toDomain(query[0]);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    throw err;
  }
};
