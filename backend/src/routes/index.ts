import express from 'express';
// import axios, { AxiosResponse } from 'axios';
// import environment from '../environment';
import { saveNew, getAll, getByID } from '../rentals/usecase';

const router = express.Router();

/* GET home page. */
router.get('/api', (req, res) => res.status(200).json({ msg: 'API' }));

router.get('/api/rentals', async (req, res) => {
  try {
    const allRentals = await getAll();
    return res.json(allRentals);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    return res.status(500).json({ error: 'Failed to get paper rentals' });
  }
});

router.get('/api/rentals/:id', async (req, res) => {
  const { id } = req.params;
  try {
    if (!id) {
      return res.status(400).json({ msg: 'Bad request: Id query param not given!' });
    }
    const rental = await getByID(Number(id));
    return res.json(rental);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    return res.status(500).json({ error: `Failed to get rental on id: ${id}` });
  }
});

router.post('/api/rentals', async (req, res) => {
  const { rental } = req.body;
  try {
    const saved = await saveNew(rental);
    return res.json({ rental: saved });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    return res.status(500).json({ err: 'Failed to POST create new rental' });
  }
});

export default router;
