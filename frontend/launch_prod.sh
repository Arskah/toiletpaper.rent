#!/bin/bash

# This script is used in the production Docker image. Not intended for use otherwise.

# Read Secret key from secret file
export REACT_APP_MAP_KEY=$(cat $SECRET_MAP_KEY_FILE)

WEB_DIR=/www

cd "${WEB_DIR}"

cat >> environment.js << EOF
window.buildEnv = {
  NODE_ENV: "production",
};
EOF

for envrow in $(printenv); do
  IFS='=' read -r key value <<< "${envrow}"
  # If there is an environment variable that starts with REACT_APP, add it to a generated
  # JS file that is served in production
  if [[ $key == REACT_APP* ]]; then
    echo "window.buildEnv.${key} = \"${value}\";" >> environment.js
  fi
done

echo "Starting Nginx..."
nginx -c /nginx.conf -g 'daemon off;'
