import React from "react";

const TPEmoji: React.FC = () => (
  <span role="img" aria-label="toilet paper">
    🧻
  </span>
);

export default TPEmoji;
