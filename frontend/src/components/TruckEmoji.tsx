import React from "react";

const TruckEmoji: React.FC = () => (
  <span role="img" aria-label="delivery truck">
    🚚
  </span>
);

export default TruckEmoji;